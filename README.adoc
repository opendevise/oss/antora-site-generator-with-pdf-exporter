= Custom Antora Site Generator with PDF Exporter
:conum-guard-ruby: #{sp}
ifdef::env-gitlab[:conum-guard-ruby: #{sp}#{sp}]
// Settings:
:experimental:
ifdef::env-gitlab[]
:toc: preamble
:!toc-title:
:toclevels: 1
endif::[]
// URLs:
:url-gs: https://www.ghostscript.com
:url-nodejs: https://nodejs.org
:url-nvm: https://github.com/nvm-sh/nvm#installing-and-updating
:url-ruby: https://ruby-lang.org
:url-rvm: https://rvm.io

This project is a custom Antora site generator that adds a PDF exporter component.
By activating this generator, you can generate and publish PDFs alongside your site.
To generate the PDFs, Antora assembles AsciiDoc source files and delegates to an AsciiDoc to PDF converter such as Asciidoctor PDF.
Antora prepares the content for the PDFs according to the structure defined by your site's navigation.

WARNING: This is alpha software!
It's highly experimental and likely to change at any time without notice.
You're free to test it and give feedback, but you *should not* rely on it in a production publishing pipeline.
Once the code reaches maturity, it will be restructured as an extended component for Antora and released for general availability.
When it's released for general availability, it will begin following the semantic versioning guidelines and you'll be able to use it for your production sites and documentation.
For now, the version number maps to a minor version number of Antora (e.g., 2.3).

== Installation

=== Directory

We highly recommend that you install this software and its prerequisites directly in the directory where your site's Antora playbook file (e.g., [.path]_antora-playbook.yml_) is located.
If you install them globally on your system, conflicts between other instances of the dependencies, Asciidoctor PDF, and core Antora components will likely occur.
The documentation for this site generator assumes that you are executing the commands from your playbook directory;
in other words, from the directory where your Antora playbook is located.

=== Prerequisites

==== Node.js

This custom Antora site generator requires Node.js (commands: `node` and `npm`).
First, install {url-nodejs}[Node.js] 12, preferably using {url-nvm}[nvm].

 $ nvm install 12

NOTE: When using nvm for Windows, you have to specify the full Node.js version, such as 12.19.0.

Run the `node` command to verify Node.js is properly installed and active:

 $ node -v

You should also be able to run the `npm` command:

 $ npm -v

==== Ruby

Asciidoctor PDF requires Ruby (commands: `ruby` and `bundle`).
Next, install {url-ruby}[Ruby] 2.7, preferably using {url-rvm}[RVM].

 $ rvm install 2.7

Run the `ruby` command to verify Ruby is properly installed and active:

 $ ruby -v

You should also be able to run the `bundle` command:

 $ bundle -v

[#ghostscript]
==== Ghostscript (Optional)

If you want to generate optimized PDFs, install {url-gs}[Ghostscript] (command: `gs` or `gswin64c`) and, if necessary, add it to your PATH.
Run the `gs` or `gswin64c` command to verify Ghostscript is properly installed:

.Linux or macOS
 $ gs -v

.Windows
 $ gswin64c -v

Instead of adding the Ghostscript command to your path, you can instead assign the location to the `GS` environment variable:

.Linux or macOS
 $ export GS=/path/to/gs

.Windows
 $env:GS = & where.exe /R 'C:\Program Files\gs' gswin64c.exe

Now that Node.js, Ruby, and optionally Ghostscript are installed, you can install the software for Antora.

=== Install the PDF Exporter

To install the custom site generator that includes the PDF Exporter, you must create a [.path]_package.json_ file and define the required Antora components.

In your playbook directory, create the file [.path]_package.json_ in your playbook directory and populate it with the following contents:

.package.json
[source,json]
----
{
  "private": true,
  "dependencies": {
    "@antora/cli": "~2.3",
    "@antora/site-generator-default": "~2.3",
    "@antora/site-generator-with-pdf-exporter": "git+https://gitlab.com/opendevise/oss/antora-site-generator-with-pdf-exporter#v2.3.0-alpha.2"
  }
}
----

Now, install the Antora CLI (command: `antora`) and generators into your project using the `npm` command from Node.js.
In your terminal, enter the following command.

 $ npm i --no-optional

IMPORTANT: The `--no-optional` flag is important as it prevents conflicting versions of the Antora packages from being installed.

To verify you've installed the site generator correctly, run the `antora` command with the version flag (`-v`):

.Linux or macOS
 $ $(npm bin)/antora -v

.Windows CMD
 $ for /f usebackq %bin in (`npm bin`) do %bin\antora -v

.Windows PowerShell
 $ cmd /c "$(npm bin)/antora -v"

These commands essentially amount to executing `node_modules/.bin/antora -v`, which you can also use.

[#install-asciidoctor-pdf]
=== Install Asciidoctor PDF

The site generator uses Asciidoctor PDF to generate PDF files.
You can install Asciidoctor PDF however you like, but we recommend using the `bundle` command to install and manage it.

In your playbook directory, create the file [.path]_Gemfile_ and populate it with the following contents:

.Gemfile
[source,ruby,subs=attributes+]
----
source 'https://rubygems.org'

gem 'asciidoctor-pdf', github: 'asciidoctor/asciidoctor-pdf', branch: 'HEAD' {conum-guard-ruby}<1>
gem 'prawn-table', github: 'prawnpdf/prawn-table', branch: 'HEAD' {conum-guard-ruby}<2>
gem 'rghost' {conum-guard-ruby}<3>
gem 'rouge' {conum-guard-ruby}<4>
----
<1> This entry requests the unreleased version of *asciidoctor-pdf* hosted on GitHub.
<2> Although *prawn-table* is an automatic dependency of Asciidoctor PDF, it must be listed explicitly since several bug fixes are only available in the unreleased version hosted on GitHub.
<3> *rghost* is the Ruby binding for Ghostscript.
<4> *rouge* provides syntax highlighting for source blocks and is the recommended option when using Asciidoctor PDF.

Now, install the *asciidoctor-pdf* gem and its dependencies into your project using the `bundle` command:

 $ bundle --path=.bundle/gems

IMPORTANT: The `--path` flag is important as it scopes the dependencies to your playbook directory (rather than installing them globally).

NOTE: On Windows, you may need to replace the forward slash in the `--path` value with a backslash (i.e., `--path=.bundle\gems`).

To verify you've installed Asciidoctor PDF correctly, run the `asciidoctor-pdf` command as follows:

 $ bundle exec asciidoctor-pdf -v

All you have left to do is to create a PDF configuration file and you'll be ready to start generating PDFs by running the custom Antora site generator with the PDF exporter enabled.

== Configuration

IMPORTANT: The Antora PDF exporter depends on your site's https://docs.antora.org/antora/latest/playbook/[Antora playbook file].
Your playbook directory must contain a playbook file (e.g., [.path]_antora-playbook.yml_) and a PDF configuration file ([.path]_pdf-config.yml_).

In addition to an Antora playbook file, the Antora PDF exporter is configured using a file named [.path]_pdf-config.yml_.
This YAML file defines the PDF configuration keys and AsciiDoc attributes that are only applied to the PDFs.
The AsciiDoc attributes defined in [.path]_pdf-config.yml_ override any matching attributes in the playbook file, component descriptor files, or pages according to Antora's https://docs.antora.org/antora/latest/playbook/asciidoc-attributes/#precedence-rules[attribute precedence rules].

Let's create a basic PDF configuration file.

. Open a new file in the text editor or IDE of your choice.
. Let's create one PDF per https://docs.antora.org/antora/latest/component-version/[component version].
In the new file, type the key name `root_level` directly followed by a colon (`:`) and a blank space.
Then type the value `1` and press kbd:[Enter] to go to the next line.
(If you want to make one PDF per component version, type the value `0` instead).
+
--
[source,yaml]
----
root_level: 1
----
--

. By default, the Antora PDF exporter generates PDFs for all of the components and versions in your site.
You can specify a range of component versions or individual component versions using the `component_versions` key.
To only generate PDFs for the https://docs.antora.org/antora/latest/how-component-versions-are-sorted/#latest-version[latest version of each component], assign the value `@*` to the key.
+
--
[source,yaml]
----
root_level: 1
component_versions: '@*'
----

The `component_versions` key accepts picomatch pattern values so you can filter what PDFs are generated by version and component name.
--

. Now, let's set some AsciiDoc attributes for the PDFs.
Attributes are mapped under the `asciidoc` and `attributes` keys in the configuration file.
These attributes will be applied to all of the PDFs according to the https://docs.antora.org/antora/latest/playbook/asciidoc-attributes/#precedence-rules[attribute precedence rules].
First, enter the `asciidoc` key, followed by a colon (`:`), and then press kbd:[Enter].
+
[source,yaml]
----
root_level: 1
component_versions: '@*'
asciidoc:
----

. The `attributes` key is nested under the `asciidoc` key.
Enter `attributes`, followed by a colon (`:`), and press kbd:[Enter].
+
[source,yaml]
----
root_level: 1
component_versions: '@*'
asciidoc:
  attributes:
----

. The individual PDF attributes are nested under the `attributes` key.
Below, let's set a built-in attribute named `source-highlighter` and assign it the value `rouge`.
This will allow non-ASCII characters to be used in the title of the PDF.
Enter the name of the attribute, followed by a colon (`:`).
Press kbd:[spacebar] once after the colon, then enter the value of the attribute.
+
[source,yaml]
----
root_level: 1
component_versions: '@*'
asciidoc:
  attributes:
    source-highlighter: rouge
----

. Let's now set up an Asciidoctor PDF theme file for customizing the PDFs the exporter generates.
Start by creating a theme file in your playbook directory named [.path]_pdf-theme.yml_.
To keep it simple, this theme extends the default theme and sets the font color on a role named "red".
This gives you something to build on later.
+
[source,yaml]
----
extends: default
role:
  red:
    font-color: #FF0000
----

. Finally, let's assign this PDF theme to the pages using the built-in `pdf-theme` attribute.
The `pdf-theme` attribute accepts the name of a YAML file stored in your playbook directory.
In this example, the file is named [.path]_pdf-theme.yml_.
+
[source,yaml]
----
root_level: 1
component_versions: '@*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
----

You're almost done!
Here's the entire PDF configuration file you've assembled so far.

.pdf-config.yml
[source,yaml]
----
root_level: 1
component_versions: '@*'
asciidoc:
  attributes:
    source-highlighter: rouge
    pdf-theme: ./pdf-theme.yml
----

This PDF configuration file will generate a PDF for the latest version of each of your components using the repository branches specified in your playbook file.
All you've got to do before running the site generator is save this file.
The file must be named [.path]_pdf-config.yml_ and saved in your playbook directory.

Once you've saved the configuration file, you're ready to run the custom site generator and export your PDFs.

== Usage

To run the Antora site generator with the PDF exporter enabled, execute the following command in your terminal from your playbook directory.

.Linux or macOS
 $ $(npm bin)/antora --clean --generator=@antora/site-generator-with-pdf-exporter antora-playbook.yml

.Windows CMD
 $ for /f usebackq %bin in (`npm bin`) do %bin\antora --clean --generator=@antora/site-generator-with-pdf-exporter antora-playbook.yml

.Windows Powershell
 $ cmd /c "(npm bin)/antora --clean --generator=@antora/site-generator-with-pdf-exporter antora-playbook.yml"

Let's breakdown this command.

. `$(npm bin)/antora` (and the like) is the base call to Antora.
The prefix `$(npm bin)/` tells the shell to use the `antora` command that is installed inside the playbook project.
. The `--clean` option flag removes all of the files and folders in the build directory prior to generating your site and PDFs.
. The `--generator` flag specifies the name of the custom site generator, `@antora/site-generator-with-pdf-exporter`.
This generator will be used instead of the default Antora generator.
. `antora-playbook.yml` is the required playbook argument.
The site generator uses the playbook file to generate your site and your PDFs.
Notice that you don't specify the name of the PDF configuration file; Antora will locate that file automatically.

Once this command is executed, each PDF will be written to the build directory.
The build directory is where the artifacts for the PDF files are assembled and the PDF files are generated.
By default, the directory location is [.path]_./build/pdf_.
This location can be customized by assigning a new value to the `build_dir` key in the [.path]_pdf-config.yml_ file.

The PDFs are also published to your site by default unless you deactivate the `publish` key in your [.path]_pdf-config.yml_ file.
The PDFs are always available for your review in the build directory, even when `publish` is assigned the value `false`.

== Optimize the PDFs

Asciidoctor PDF does not generate optimized PDFs by default.
This can result in PDF files that are larger than they need to be.
To reduce the file size, you can activate the https://github.com/asciidoctor/asciidoctor-pdf#optimizing-the-generated-pdf[PDF optimizer in Asciidoctor PDF].
The optimizer will use Ghostscript via rghost by default, or you can plug in a custom optimizer to use hexapdf.

=== Ghostscript via rghost

To optimize your PDF using rghost, you must <<ghostscript,install Ghostscript>> (and have the *rghost* gem installed, as recommended above).
Additionally, the `optimize` attribute must be set under the `attributes` key.
For maximum optimization, assign it the value `prepress`.

.pdf-config.yml
[source,yaml]
----
asciidoc:
  attributes:
    optimize: prepress
----

If rghost cannot locate the Ghostscript command, which is typical on Windows, you'll need to specify the location of the command using the `GS` environment variable.
See <<ghostscript>> for details.

If you don't want to rely on a system command, or are looking for better optimizaation, you can use hexapdf by plugging in your own optimizer.

=== hexapdf

Another option to optimize the PDF is https://hexapdf.gettalong.org/[hexapdf].
The benefit is that it is a pure Ruby application, so you don't need to worry about installing an extra system command.
However, before introducing it, though, it's important to point out that its license is AGPL.
If that's okay with you, read on to learn how to use it.

First, add the *hexapdf* gem to your [.path]_Gemfile_:

[source,ruby]
----
gem 'hexapdf'
----

Next, run the `bundle` command again to install it into your project:

 $ bundle

Now you'll need to configure the optimizer to prefer hexapdf over rghost by providing your own implementation of the `Optimizer` class.
Start by creating a Ruby file named [.path]_optimizer-hexapdf.rb_ and populate it with the following code:

.optimizer-hexapdf.rb
[source,ruby]
----
require 'hexapdf/cli'

class Asciidoctor::PDF::Optimizer
  def initialize(*)
    app = HexaPDF::CLI::Application.new
    app.instance_variable_set :@force, true
    @optimize = app.main_command.commands['optimize']
  end

  def optimize_file path
    options = @optimize.instance_variable_get :@out_options
    options.compress_pages = true
    @optimize.execute path, path
    nil
  rescue
    # retry without page compression, which can sometimes fail
    options.compress_pages = false
    @optimize.execute path, path
    nil
  end
end
----

To activate your custom optimizer, you must pass this script to Asciidoctor PDF by adding the `-r` flag to the value of the `command` key in [.path]_pdf-config.yml_:

.pdf-config.yml
[source,yaml]
----
command: bundle exec asciidoctor-pdf -r ./optimizer-hexapdf.rb
----

You'll also need to set the `optimize` attribute under the `attributes` key.
The value of this attribute can be blank since `hexapdf` doesn't offer different optimization modes:

.pdf-config.yml
[source,yaml]
----
asciidoc:
  attributes:
    optimize: ''
----

Now Asciidoctor PDF will automatically optimize the PDFs it generates by running hexapdf on them.

The benefit hexapdf has over Ghostscript is that it does not resample any of the images or rasterize the text.
That means you end up with the highest fidelity PDF while ensuring that the PDF file only contains the necessary data.

=== Disable PDF optimization

If you don't want to optimize the PDFs, remove the `optimize` attribute defined in [.path]_pdf-config.yml_.

== Reference

=== PDF Configuration Keys

The table below lists the keys that can be defined in [.path]_pdf-config.yml_.

[cols="2,6,2,2"]
|===
|Name |Description |Default |Values

|`build_dir`
|The build directory where the artifacts for the PDF files are assembled and the PDF files are generated.
|`./build/pdf`
|Filesystem path

|`clean`
|A switch that controls whether the build directory is cleaned before use. Inherits its value from the Antora playbook if not explicitly set.
Also see the https://docs.antora.org/antora/latest/cli/options/#clean[--clean option].
|`false`
|Boolean

|`command`
|The command that calls Asciidoctor PDF to convert the compiled AsciiDoc documents to PDFs.
Here you can pass additional options to Asciidoctor PDF, such as `-v` or `-r` (e.g., `bundle exec asciidoctor-pdf -v`).
|`bundle exec asciidoctor-pdf`
|Command string

|`component_versions`
a|A filter that specifies which component versions the generator exports to PDFs.
The value(s) can be one or more picomatch patterns.

* `*` matches all components and versions.
* `@*` matches only the latest version of all components.
* `@<component-name>` matches the latest version of the specified component.
* `@{<component-name-1>,<component-name-2>}` matches the latest version of two different components.
* `*@<component-name>` matches all versions of the specified component.
* ...and so on

|`*`
|Array of picomatch patterns

|`disable`
|A switch that disables the PDF exporter even when the custom generator is in use.
|`false`
|Boolean

|`insert_start_page`
|A switch that controls whether the component version start page is inserted into the top of the PDF if it isn't specified in the navigation file.
|`true`
|Boolean

|`keep_generated_sources`
|A switch that controls whether the compiled AsciiDoc files are kept in the build directory.
This key is useful for debugging.
|`false`
|Boolean

|`process_limit`
|The number of concurrent processes to the generator should not exceed when converting the compiled AsciiDoc documents to PDFs.
|`1`
|A number

|`publish`
|A switch that controls whether the exported PDFs are included in the published site.
The PDFs are published to the root directory of the corresponding component version.
The PDFs are always available under the build folder.
|`true`
|Boolean

|`root_level`
a|The navigation list entry level from which to start making PDFs (`0` or `1`).

* `0` makes one PDF per component version.
* `1` makes a PDF per top-level entry in the navigation tree of each component version.

|`1`
|`0` or `1`

|`section_merge_strategy`
a|Determines how the generator merges sections from the page content with sections created to represent navigation entries.

* `discrete` labels all sections in the content as discrete headings.
* `fuse` preserves sections from the page content, inserting sections from a parent entry as the first children in the TOC.
* `enclose` acts like `fuse`, except it encloses sections from a parent entry in a generated section, which then becomes the first child of that parent in the TOC.
The title of this section is controlled by the `overview-title` AsciiDoc attribute.

|`discrete`
|`discrete`, `fuse`, or `enclose`

|`asciidoc.attributes`
|AsciiDoc document attributes that are applied to each compiled AsciiDoc document when exporting to PDF.
These attributes supplement attributes defined in the playbook, component version descriptors, and pages.
The `pdf-theme` attribute can be used to specify the theme for Asciidoctor PDF.
See https://docs.antora.org/antora/2.3/playbook/asciidoc-attributes/[Assign Attributes to a Site] for usage examples and precedence rules.
|`doctype: book`
|Map of built-in and custom AsciiDoc attributes
|===

=== PDF Theme

The PDF theme, if used, is specified by the `pdf-theme` attribute defined in [.path]_pdf-config.yml_.
It's set and assigned under the `attributes` key.

.pdf-config.yml
[source,yaml]
----
asciidoc:
  attributes:
    pdf-theme: ./pdf-theme.yml
----

The PDF theme can be configured according to the https://github.com/asciidoctor/asciidoctor-pdf/blob/master/docs/theming-guide.adoc[Asciidoctor PDF theming guide].

== Update the Software

Since the Antora PDF Exporter isn't released as a package yet, you'll need to fetch the latest updates directly from the development branch of its git repository.

Use the following command from your playbook directory to fetch the latest updates to the Antora PDF Exporter.

 $ rm -rf node_modules package-lock.json && npm i --no-optional

This command forces the development branch to be recloned.

Here are the equivalent commands for the Windows command prompt:

 RD /S /Q node_modules
 DEL /F /Q package-lock.json
 npm i --no-optional

You don't need to make any changes to your [.path]_package.json_ or [.path]_Gemfile_ when you get the latest site generator updates.

== Contributing

=== Installation for Development

 $ npm install -g gitlab:opendevise/oss/antora-site-generator-with-pdf-exporter

or

 $ npm link

You will also need Asciidoctor PDF, which you must install using Bundler (cmd: `bundle`).
Follow the instructions in <<install-asciidoctor-pdf>> for creating a [.path]_Gemfile_ that you can use to install Asciidoctor PDF and its dependencies .

=== Usage

 $ NODE_PATH="$(npm -g root)" antora --generator @antora/site-generator-with-pdf-exporter antora-playbook.yml

NOTE: The `NODE_PATH` assignment is necessary to ensure Antora can locate node modules install globally.
Depending on your environment, you may find that this assignment is unnecessary.

The PDFs will be generated into the build directory.

== Copyright and License

Copyright (C) 2020 OpenDevise Inc.

Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.
