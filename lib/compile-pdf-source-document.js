'use strict'

const loadAsciiDoc = require('@antora/asciidoc-loader')
const File = require('vinyl')
const { posix: path } = require('path')

function compilePdfSourceDocument (
  contentCatalog,
  asciidocConfig,
  mutableAttributes,
  pages,
  componentVersion,
  pdfOutline,
  sectionMergeStrategy
) {
  const pagesInOutline = collectPagesInOutline(pages, pdfOutline)
  const navtitle = pdfOutline.content
  const stem = generatePdfStem(componentVersion, navtitle)
  const header = buildAsciiDocHeader(componentVersion, navtitle)
  const body = assembleAsciiDocForEntry(
    contentCatalog,
    asciidocConfig,
    mutableAttributes,
    componentVersion,
    pagesInOutline,
    pdfOutline,
    sectionMergeStrategy,
    header
  )
  const relativeSrcPath = `${stem}.adoc`
  return new File({
    asciidocConfig,
    contents: Buffer.from([...header, ...body].join('\n') + '\n'),
    mediaType: 'text/asciidoc',
    path: relativeSrcPath,
    src: {
      component: componentVersion.name,
      version: componentVersion.version,
      basename: path.basename(relativeSrcPath),
      stem,
      extname: '.adoc',
    },
  })
}

function buildAsciiDocHeader (componentVersion, navtitle) {
  const doctitle = navtitle === componentVersion.title ? navtitle : `${componentVersion.title}: ${navtitle}`
  return [
    `= ${doctitle}`,
    ...(componentVersion.version === 'master' ? [] : [`v${componentVersion.version}`]),
    ':doctype: book', // for debugging only; set via CLI
    // Q: should we pass these via the CLI so they cannot be modified?
    `:page-component-name: ${componentVersion.name}`,
    `:page-component-version: ${componentVersion.version}`,
    ':page-version: {page-component-version}',
    `:page-component-display-version: ${componentVersion.displayVersion}`,
    `:page-component-title: ${componentVersion.title}`,
  ]
}

function collectPagesInOutline (pages, outlineEntry) {
  const page = outlineEntry.urlType === 'internal' ? pages.find((it) => it.pub.url === outlineEntry.url) : undefined
  return (outlineEntry.items || []).reduce(
    (accum, item) => new Map([...accum, ...collectPagesInOutline(pages, item)]),
    new Map(
      page && [
        [`${page.src.module === 'ROOT' ? '' : page.src.module + ':'}${page.src.relative}`, page],
        [page.pub.url, page],
      ]
    )
  )
}

function assembleAsciiDocForEntry (
  contentCatalog,
  asciidocConfig,
  mutableAttributes,
  componentVersion,
  pagesInOutline,
  outlineEntry,
  sectionMergeStrategy,
  header,
  level = 0
) {
  const buffer = []
  // TODO: we could try to be smart about it and make sure the page with fragment is included at least once
  if (outlineEntry.hash) return buffer
  const { content: navtitle, items, unresolved, urlType, url } = outlineEntry
  // FIXME: ideally, resource ID would be stored in navigation so we can look up the page more efficiently
  let page = urlType === 'internal' && !unresolved ? pagesInOutline.get(url) : undefined
  if (page) {
    let contents = page.src.contents
    // NOTE: blank lines at top and bottom of document create mismatch when using line numbers to navigate source lines
    // IMPORTANT: this must not leave behind lines the parser will drop!
    // IDEA: another option is to capture initial lineno of reader and use as offset (but preseves those blank lines)
    contents = Buffer.from(
      contents
        .toString()
        .replace(/^(?:[ \t]*\r\n?|[ \t]*\n)+/, '')
        .trimRight()
    )
    page = new page.constructor(Object.assign({}, page, { contents, mediaType: 'text/asciidoc' }))
    const { module: module_, relative, origin } = page.src
    const doc = loadAsciiDoc(page, contentCatalog, asciidocConfig)
    const ids = doc.getCatalog().ids
    // NOTE: in Antora, docname is relative src path from module without file extension
    const docname = doc.getAttribute('docname')
    //const idprefix = `${module_ === 'ROOT' ? '' : module_ + ':'}${docname}//`
    const idprefix = `${module_ === 'ROOT' ? '' : module_ + ':'}${docname.replace(/[/]/g, '::')}:::`
    //const idprefix = `${module_ === 'ROOT' ? '' : module_ + ':'}${docname.replace(/[/]/g, ':-:')}:--:`
    buffer.push('')
    buffer.push(`:docname: ${docname}`)
    buffer.push(`:page-module: ${module_}`)
    buffer.push(`:page-relative-src-path: ${relative}`)
    //buffer.push(`:page-origin-type: ${origin.type}`)
    buffer.push(`:page-origin-url: ${origin.url}`)
    buffer.push(`:page-origin-start-path: ${origin.startPath}`)
    buffer.push(`:page-origin-refname: ${origin.branch || origin.tag}`)
    buffer.push(`:page-origin-reftype: ${origin.branch ? 'branch' : 'tag'}`)
    buffer.push(`:page-origin-refhash: ${origin.worktree ? '(worktree)' : origin.refhash}`)
    let enclosed
    // NOTE: if level is 0, doctitle has already been added and we're in the document header
    if (level) {
      if (level === 1 && navtitle === componentVersion.title) {
        level--
      } else {
        let hlevel = level + 1
        if (hlevel > 6) {
          hlevel = 6
          buffer.push(`[discrete#${idprefix}]`)
        } else {
          buffer.push(`[#${idprefix}]`)
        }
        buffer.push(`${'='.repeat(hlevel)} ${navtitle}`)
      }
    } else {
      header.unshift(`[#${idprefix}]`)
    }
    if (sectionMergeStrategy === 'enclose' && items && doc.hasSections()) {
      enclosed = true
      // TODO: make overview section title configurable
      //let overviewTitle = doc.getDocumentTitle()
      //if (overviewTitle === navtitle) overviewTitle = doc.getAttribute('overview-title', 'Overview')
      const overviewTitle = doc.getAttribute('overview-title', 'Overview')
      buffer.push('')
      // NOTE: try to toggle sectids; otherwise, fallback to globally unique synthetic ID
      let toggleSectids, syntheticId
      if (doc.isAttribute('sectids')) {
        if (doc.isAttributeLocked('sectids')) {
          syntheticId = `__object-id-${getObjectId(outlineEntry)}`
        } else {
          buffer.push(':!sectids:')
          toggleSectids = true
        }
      }
      let hlevel = level + 2
      if (hlevel > 6) {
        hlevel = 6
        buffer.push(syntheticId ? `[discrete#${syntheticId}]` : '[discrete]')
      } else if (syntheticId) {
        buffer.push(`[#${syntheticId}]`)
      }
      buffer.push(`${'='.repeat(hlevel)} ${overviewTitle}`)
      if (toggleSectids) buffer.push(':sectids:')
    }
    const siteUrl = doc.getAttribute('site-url')
    const lines = doc.getSourceLines()
    const ignoreLines = []
    // TODO: think more about when multipart is allowed; perhaps configurable
    if (doc.hasSections()) fixSectionLevels(doc.getSections(), level === 0)
    // TODO: update / simplify findBy calls when upgrading to Asciidoctor 2
    const allBlocks = doc
      .findBy((it) => it.getContext() !== 'document')
      .reduce((accum, block) => {
        accum.push(block)
        if (block.getContext() === 'table') {
          const rows = block.rows
          ;[...rows.body, ...rows.foot].forEach((row) => {
            row.forEach((cell) => {
              if (cell.style !== 'asciidoc') return
              accum.push(...cell.inner_document.findBy((it) => it.getContext() !== 'document'))
            })
          })
        }
        return accum
      }, [])
    allBlocks.forEach((block) => {
      const contentModel = block.content_model
      if (
        !block.hasSubstitution('macros') &&
        (contentModel === 'verbatim' || contentModel === 'simple' || contentModel === 'pass')
      ) {
        const idx = block.$lineno() - 1
        const startLine = lines[idx]
        // NOTE: one case this happens if when coalescer messes up includes
        if (startLine == null) {
          console.log(`null startLine for ${block.getContext()} at ${idx} in ${relative}`)
          return
        }
        const char0 = startLine.charAt()
        // FIXME: needs to be more robust; move logic to helper
        const delimited =
          startLine.length > 3 &&
          startLine === char0.repeat(startLine.length) &&
          (char0 === '-' || char0 === '.' || char0 === '+')
        // QUESTION: exclude block attribute lines too? what about attribute entries?
        for (let i = idx; i < block.lines.length + (delimited ? idx + 2 : idx); i++) ignoreLines.push(i)
      }
    })
    for (let idx = 0, len = lines.length; idx < len; idx++) {
      if (~ignoreLines.indexOf(idx)) continue
      let line = lines[idx]
      if (~line.indexOf('<<')) {
        line = line.replace(/(?<![\\+])<<#?([\p{Alpha}0-9_/.:{][^>,]*?)(?:|, *([^>]+?))?>>/gu, (m, refid, text) => {
          // support natural xref; note this logic will change when upgrading to Asciidoctor 2
          if (!ids['$key?'](refid) && (~refid.indexOf(' ') || refid.toLowerCase() !== refid)) {
            if (
              !(refid = doc
                .getCatalog()
                .ids.$key(refid)
                .$to_s())
            ) {
              return m
            }
          }
          return `<<${idprefix}${refid}${text ? ',' + text : ''}>>`
        })
      }
      // NOTE: the next check takes care of inline and block anchors
      if (~line.indexOf('[[')) {
        line = line.replace(/\[\[([\p{Alpha}_:][\p{Alpha}0-9_\-:.]*)(|, *.+?)\]\]/gu, `[[${idprefix}$1$2]]`)
      }
      if (~line.indexOf('xref:')) {
        line = line.replace(/xref:([\p{Alpha}#/.:{].*?)\[(|.*?[^\\])\]/gu, (m, target, text) => {
          let pagePart, fragment, targetPage
          const hashIdx = target.indexOf('#')
          if (~hashIdx) {
            pagePart = target.substr(0, hashIdx)
            fragment = target.substr(hashIdx + 1)
            // TODO: for now, assume .adoc; in the future, consider other file extensions
            if (!(pagePart && pagePart.endsWith('.adoc'))) pagePart += '.adoc'
          } else if (target.endsWith('.adoc')) {
            pagePart = target
            fragment = ''
          } else {
            fragment = target
          }
          if (!pagePart) return `<<${idprefix}${fragment}${text ? ',' + text.replace(/\\]/g, ']') : ''}>>`
          if (~pagePart.indexOf('@') || /:.*:/.test(pagePart)) {
            // TODO: handle unresolved page better
            return siteUrl && (targetPage = contentCatalog.resolvePage(pagePart, page.src))
              ? `${siteUrl}${targetPage.pub.url}${fragment && '#' + fragment}[${text}]`
              : m
          } else if (pagePart.indexOf(':') < 0) {
            if (module_ !== 'ROOT') pagePart = `${module_}:${pagePart}`
          } else if (pagePart.startsWith('ROOT:')) {
            pagePart = pagePart.substr(5)
          }
          if (!(targetPage = pagesInOutline.get(pagePart))) {
            // TODO: handle unresolved page better
            return siteUrl && (targetPage = contentCatalog.resolvePage(target, page.src))
              ? `${siteUrl}${targetPage.pub.url}${fragment && '#' + fragment}[${text}]`
              : m
          }
          pagePart = pagePart.replace(/[/]/g, '::').replace(/\.adoc$/, '')
          const refid = `${pagePart}:::${fragment}`
          return `<<${refid}${text && text !== targetPage.title ? ',' + text.replace(/\\]/g, ']') : ''}>>`
        })
      }
      if (~line.indexOf('link:{attachmentsdir}/')) {
        line = line.replace(/(?<![\\+])link:\{attachmentsdir\}\/([^\s[]+)\[(|.*?[^\\])\]/g, (m, relative, text) => {
          const attachment =
            siteUrl &&
            contentCatalog.getById({
              component: componentVersion.name,
              version: componentVersion.version,
              module: module_,
              family: 'attachment',
              relative,
            })
          return attachment ? `${siteUrl}${attachment.pub.url}[${text}]` : m
        })
      }
      if (~line.indexOf('image:') && !line.startsWith('image::')) {
        line = line.replace(/(?<![\\+])image:([^:\s[](?:[^[]*[^\s[])?)\[([^\]]*)\]/g, (m, target, attrlist) => {
          if (!isUrl(target)) {
            const image = contentCatalog.resolveResource(target, page.src, 'image', ['image'])
            // TODO: handle (or report) unresolved image better
            if (image) {
              image.out.inPdf = true
              return `image:${image.pub.url.substr(1)}[${attrlist}]`
            }
          }
          return m
        })
      }
      lines[idx] = line
    }
    // NOTE: need to do this last since it modifies the line numbers
    // we could remap the line numbers to make them resilient
    // or we could mark which lines to remove and filter them after
    ;[...allBlocks].reverse().forEach((block) => {
      const lineno = block.$lineno()
      // NOTE: lineno is not defined for preamble
      if (typeof lineno !== 'number') return
      const context = block.getContext()
      const idx = lineno - 1
      if (context === 'section') {
        if (block.getSectionName() === 'header') {
          lines.splice(idx, 1)
          return
        }
        let blockStyle = sectionMergeStrategy === 'discrete' ? 'discrete' : undefined
        lines[idx] = lines[idx].replace(/^=+( .+)/, (_, rest) => {
          let targetMarkerLength = block.level + 1 + level + (enclosed ? 1 : 0)
          if (targetMarkerLength > 6) {
            targetMarkerLength = 6
            blockStyle = 'discrete'
          }
          return '='.repeat(targetMarkerLength) + rest
        })
        // NOTE: ID will be undefined if sectids are turned off
        if (block.getId()) rewriteStyleAttribute(block, lines, idx, idprefix, blockStyle)
      } else {
        if (context === 'image') {
          const atImageMacro = (lines[idx] || '').startsWith('image::')
          // NOTE: the following logic is needed only if parser is messing up line number of image block
          //let atImageMacro
          //// NOTE: account for line number tracking error in parser when image has block anchor
          //if (block.getId()) {
          //  const originalIdx = idx
          //  let line
          //  while ((line = lines[idx]) != null && !(atImageMacro = line.startsWith('image::'))) idx++
          //  if (!atImageMacro) {
          //    idx = originalIdx
          //    if ((lines[idx - 1] || '').startsWith('image::')) {
          //      atImageMacro = true
          //      idx--
          //    }
          //  }
          //} else if ((lines[idx] || '').startsWith('image::')) {
          //  atImageMacro = true
          //}
          const target = block.getAttribute('target')
          if (atImageMacro && !isUrl(target)) {
            const image = contentCatalog.resolveResource(target, page.src, 'image', ['image'])
            // FIXME: handle (or report) case when image is not resolved
            if (image) {
              image.out.inPdf = true
              const line = lines[idx]
              lines[idx] = `image::${image.pub.url.substr(1)}${line.substr(line.indexOf('['))}`
            }
          }
        }
        if (block.getId()) rewriteStyleAttribute(block, lines, idx, idprefix)
      }
    })
    buffer.push(...lines)
    const attributeEntries = Object.entries(doc.attributes_defined_in_header)
    if (attributeEntries.length) {
      const resolvedAttributeEntries = attributeEntries.reduce(
        (accum, [name, val]) => {
          // Q: couldn't we just check if attribute is locked?
          if (name in mutableAttributes) {
            const initialVal = mutableAttributes[name]
            if (initialVal == null) {
              if (val != null) accum.push(`:!${name}:`)
            } else if (val !== initialVal) {
              accum.push(`:${name}:${initialVal ? ' ' + initialVal : ''}`)
            }
          } else if (val != null && !doc.isAttributeLocked(name)) {
            accum.push(`:!${name}:`)
          }
          return accum
        },
        ['']
      )
      if (resolvedAttributeEntries.length > 1) buffer.push(...resolvedAttributeEntries)
    }
  } else {
    if (level) {
      if (level === 1 && navtitle === componentVersion.title) {
        level--
      } else {
        buffer.push('')
        // NOTE: try to toggle sectids; otherwise, fallback to globally unique synthetic ID
        let toggleSectids, syntheticId
        if (!('sectids' in asciidocConfig.attributes)) {
          buffer.push(':!sectids:')
          toggleSectids = true
        } else if (typeof asciidocConfig.attributes.sectids === 'string') {
          if ('sectids' in mutableAttributes) {
            buffer.push(':!sectids:')
            toggleSectids = true
          } else {
            syntheticId = `__object-id-${global.Opal.hash(outlineEntry).$object_id()}`
          }
        }
        // Q: should we unset docname, page-module, etc?
        const sectionTitle = urlType === 'external' ? `${url}[${navtitle.replace(/\]/, '\\]')}]` : navtitle
        let hlevel = level + 1
        if (hlevel > 6) {
          hlevel = 6
          buffer.push(syntheticId ? `[discrete#${syntheticId}]` : '[discrete]')
        } else if (syntheticId) {
          buffer.push(`[#${syntheticId}]`)
        }
        buffer.push(`${'='.repeat(hlevel)} ${sectionTitle}`)
        if (toggleSectids) buffer.push(':sectids:')
      }
    }
  }

  const nextLevel = level + 1
  if (items) {
    items.forEach((item) => {
      buffer.push(
        ...assembleAsciiDocForEntry(
          contentCatalog,
          asciidocConfig,
          mutableAttributes,
          componentVersion,
          pagesInOutline,
          item,
          sectionMergeStrategy,
          header,
          nextLevel
        )
      )
    })
  }
  return buffer
}

function generatePdfStem (componentVersion, title) {
  const segments = [componentVersion.name]
  if (componentVersion.version !== 'master') segments.push(componentVersion.version)
  segments.push(
    title
      .toLowerCase()
      .replace(/&.+?;|[^ \p{Alpha}0-9_\-.]/gu, '')
      .replace(/[ _.]/g, '-')
      .replace(/--+/g, '-')
  )
  return path.join(...segments)
}

function fixSectionLevels (sections, multipart) {
  sections.forEach((sect) => {
    const targetLevel = sect.getParent().getLevel() + 1
    if (multipart ? sect.getLevel() > targetLevel : sect.getLevel() !== targetLevel) sect.level = targetLevel
    if (sect.hasSections()) fixSectionLevels(sect.getSections(), multipart)
  })
}

function rewriteStyleAttribute (block, lines, idx, idprefix, replacementStyle = '') {
  let prevLine = lines[idx - 1]
  const char0 = prevLine && prevLine.charAt()
  if (char0) {
    if (
      (char0 === '.' && /^\.\.?[^ \t.]/.test(prevLine)) ||
      (char0 === '[' &&
        prevLine.charAt(1) === '[' &&
        /^\[\[(?:|[\p{Alpha}_:][\p{Alpha}0-9_\-:.]*(?:, *.+)?)\]\]$/u.test(prevLine))
    ) {
      return rewriteStyleAttribute(block, lines, idx - 1, idprefix, replacementStyle)
    }
  }
  let cellSpec
  if (
    char0 &&
    (char0 === '[' || (block.getDocument().isNested() && (cellSpec = prevLine.match(/^([^[|]*)\| *(\[.+)/)))) &&
    prevLine.charAt(prevLine.length - 1) === ']'
  ) {
    if (cellSpec) {
      prevLine = cellSpec[2]
      cellSpec = cellSpec[1]
    }
    let rawStyle
    const commaIdx = prevLine.indexOf(',')
    if (~commaIdx) {
      rawStyle = prevLine.substr(1, commaIdx - 1)
      if (~rawStyle.indexOf('=')) rawStyle = undefined
    } else if (!~prevLine.indexOf('=')) {
      rawStyle = prevLine.substr(1, prevLine.length - 2)
    }
    if (rawStyle) {
      if (~rawStyle.indexOf('#')) {
        prevLine = prevLine.replace(/#[^.%,\]]+/, `#${idprefix}${block.getId()}`)
        if (replacementStyle) prevLine = prevLine.replace(/^[^#.%,\]]+/, `[${replacementStyle}`)
      } else {
        prevLine = `[${
          replacementStyle ? rawStyle.replace(/^[^.%]*]/, replacementStyle) : rawStyle
        }#${idprefix}${block.getId()}${prevLine.substr(rawStyle.length + 1)}`
      }
    } else {
      prevLine = `[${replacementStyle}#${idprefix}${block.getId()}${rawStyle == null ? ',' : ''}${prevLine.substr(1)}`
    }
    if (cellSpec) prevLine = `${cellSpec}|${prevLine}`
    lines[idx - 1] = prevLine
  } else {
    lines.splice(idx, 0, `[${replacementStyle}#${idprefix}${block.getId()}]`)
  }
}

function isUrl (str) {
  return ~str.indexOf(':') && /^\p{Alpha}[\p{Alpha}0-9.+-]+:[/]{0,2}/u.test(str)
}

function getObjectId (obj) {
  return global.Opal.uid()
}

module.exports = compilePdfSourceDocument
