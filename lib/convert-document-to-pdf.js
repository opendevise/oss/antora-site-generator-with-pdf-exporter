'use strict'

const asciidoctorPdf = require('./asciidoctor-pdf')
const ospath = require('path')

function convertDocumentToPdf (pdfConfig, pdfDoc) {
  const {
    asciidocConfig,
    contents: source,
    src: { basename, component, extname: docfilesuffix, version },
  } = pdfDoc
  const docfile = `${version}@${component}::pdf$${basename}`
  const docname = basename.substr(0, basename.length - docfilesuffix.length) + '@'
  const imagesdir = pdfConfig.buildDir
  const pdfAttributes = Object.assign({}, asciidocConfig.attributes, { docfile, docfilesuffix, docname, imagesdir })
  const options = { cmd: pdfConfig.command, cwd: pdfConfig.cwd }
  pdfDoc.contents = null
  pdfDoc.extname = '.pdf'
  pdfDoc.mediaType = 'application/pdf'
  return asciidoctorPdf(source, ospath.join(pdfConfig.buildDir, pdfDoc.path), pdfAttributes, options).then(
    (contents) => {
      pdfDoc.contents = contents
      pdfDoc.out = { path: pdfDoc.path }
      return pdfDoc
    }
  )
}

module.exports = convertDocumentToPdf
