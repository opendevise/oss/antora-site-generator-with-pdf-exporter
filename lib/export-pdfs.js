'use strict'

const asciidocSourceCoalescerExtension = require('./asciidoc-source-coalescer-extension')
const compilePdfSourceDocument = require('./compile-pdf-source-document')
const convertDocumentToPdf = require('./convert-document-to-pdf')
const { isMatch } = require('picomatch')
const loadAsciiDoc = require('@antora/asciidoc-loader')
const loadPdfConfig = require('./load-pdf-config')
const promiseAllWithLimit = require('./promise-all-with-limit')
const publishFiles = require('@antora/site-publisher')

const PICOMATCH_OPTS = { nobracket: true, noextglob: true, noglobstar: true, noquantifiers: true }

async function exportPdfs (playbook, contentCatalog, siteAsciiDocConfig, navigationCatalog, pages) {
  const pdfConfig = await loadPdfConfig(playbook)
  if (!pdfConfig) return []
  const { componentVersions: componentVersionFilter, insertStartPage, rootLevel, sectionMergeStrategy } = pdfConfig

  // NOTE: the extensions property on each scoped AsciiDoc config is current mapped to the extensions property on the
  // site-wide AsciiDoc config
  const pdfSourceDocs = withAsciiDocExtension(
    asciidocSourceCoalescerExtension,
    contentCatalog,
    siteAsciiDocConfig,
    () =>
      contentCatalog.getComponents().reduce((accum, component) => {
        const latestComponentVersion = component.latest
        component.versions.forEach((componentVersion) => {
          const { name: componentName, version } = componentVersion
          const navigation = navigationCatalog.getNavigation(componentName, version)
          if (!navigation) return
          // TODO: move filtering to a function
          if (
            componentVersionFilter &&
            !(
              componentVersionFilter.length === 1 &&
              (componentVersionFilter[0] === '*' ||
                (componentVersionFilter[0] === '@*' && componentVersion === latestComponentVersion))
            )
          ) {
            const signature = [`${version}@${componentName}`]
            // Q: should we use a reserved work for latest version, like %latest% or _latest?
            if (componentVersion === latestComponentVersion) signature.push(`@${componentName}`)
            if (!signature.some((it) => isMatch(it, componentVersionFilter, PICOMATCH_OPTS))) return
          }
          const asciidocConfig = Object.assign({}, componentVersion.asciidoc, {
            attributes: Object.assign({}, componentVersion.asciidoc.attributes, pdfConfig.asciidoc.attributes),
          })
          const startPage = contentCatalog.resolvePage('index.adoc', { component: componentName, version })
          let startPageUrl
          if (startPage && insertStartPage) {
            if (inNav(navigation, (startPageUrl = startPage.pub.url))) startPageUrl = undefined
          }
          const mutableAttributes = startPage ? computeMutableAttributes(contentCatalog, asciidocConfig, startPage) : {}
          const singleRoot = navigation.length === 1
          // Q: should we be bundling nav with single root together when root_level === 1?
          if (rootLevel === 0 || singleRoot) {
            const navEntry = startPageUrl
              ? { content: componentVersion.title, url: startPageUrl, urlType: 'internal' }
              : { content: componentVersion.title }
            Object.assign(
              navEntry,
              singleRoot
                ? navigation[0]
                : {
                    items: navigation.reduce((navTree, it) => {
                      it.content ? navTree.push(it) : navTree.push(...it.items)
                      return navTree
                    }, []),
                  }
            )
            accum.push(
              compilePdfSourceDocument(
                contentCatalog,
                asciidocConfig,
                mutableAttributes,
                pages,
                componentVersion,
                navEntry,
                sectionMergeStrategy
              )
            )
          } else {
            navigation
              .reduce(
                (navTree, it) => {
                  it.content ? navTree.push(it) : navTree.push(...it.items)
                  return navTree
                },
                startPageUrl ? [{ content: componentVersion.title, url: startPageUrl, urlType: 'internal' }] : []
              )
              .forEach((navEntry) =>
                accum.push(
                  compilePdfSourceDocument(
                    contentCatalog,
                    asciidocConfig,
                    mutableAttributes,
                    pages,
                    componentVersion,
                    navEntry,
                    sectionMergeStrategy
                  )
                )
              )
          }
        })
        return accum
      }, [])
  )
  // piggyback on core publishSite function (mapped to publishFiles) to publish images to PDF build directory ;)
  return publishFiles({ output: { clean: pdfConfig.clean, dir: pdfConfig.buildDir } }, [
    {
      getFiles: () => {
        const pdfBuildFiles = contentCatalog.findBy({ family: 'image' }).filter(({ out }) => out && out.inPdf)
        if (pdfConfig.keepGeneratedSource) {
          pdfSourceDocs.forEach((it) => (it.out = { path: it.path }))
          pdfBuildFiles.push(...pdfSourceDocs)
        }
        return pdfBuildFiles
      },
    },
  ])
    .then(() => promiseAllWithLimit(convertDocumentToPdf.bind(null, pdfConfig), pdfSourceDocs, pdfConfig.processLimit))
    .then((pdfDocs) => (pdfConfig.publish ? pdfDocs : []))
}

function withAsciiDocExtension (extension, contentCatalog, siteAsciiDocConfig, fn = () => {}) {
  initAsciiDocExtensions(contentCatalog, siteAsciiDocConfig).push(extension)
  try {
    return fn()
  } finally {
    siteAsciiDocConfig.extensions.pop()
  }
}

function initAsciiDocExtensions (contentCatalog, asciidocConfig) {
  if (asciidocConfig.extensions) return asciidocConfig.extensions
  return contentCatalog.getComponents().reduce((extensions, { versions }) => {
    versions.forEach((componentVersion) => (componentVersion.asciidoc.extensions = extensions))
    return extensions
  }, (asciidocConfig.extensions = []))
}

function computeMutableAttributes (contentCatalog, asciidocConfig, referencePage) {
  const doc = loadAsciiDoc(
    new referencePage.constructor(
      Object.assign({}, referencePage, { contents: Buffer.alloc(0), mediaType: 'text/asciidoc' })
    ),
    contentCatalog,
    asciidocConfig
  )
  const additionalMutableNames = [
    'page-component-name',
    'page-component-version',
    'page-version',
    'page-component-display-version',
    'page-component-title',
  ]
  // we could consider using an Asciidoctor extension here to grab attributes passed via the API instead
  const immutableAttributeNames = doc.attribute_overrides.$keys()['$-'](additionalMutableNames)
  return Object.entries(doc.getAttributes()).reduce((accum, [name, val]) => {
    if (!immutableAttributeNames.includes(name)) accum[name] = val
    return accum
  }, {})
}

function inNav (items, url) {
  return items.find((it) => it.url === url || inNav(it.items || [], url))
}

module.exports = exportPdfs
