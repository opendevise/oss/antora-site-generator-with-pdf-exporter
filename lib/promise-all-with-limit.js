'use strict'

async function promiseAllWithLimit (fn, items, limit = Infinity) {
  const result = []
  const active = []
  for (const item of items) {
    const pending = Promise.resolve().then(() => fn(item))
    result.push(pending)
    if (limit > items.length) continue
    const proceeding = pending.then(() => active.splice(active.indexOf(proceeding), 1))
    active.push(proceeding)
    if (limit > active.length) continue
    await Promise.race(active)
  }
  return Promise.all(result)
}

module.exports = promiseAllWithLimit
